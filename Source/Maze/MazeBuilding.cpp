#include <iostream>
#include "MazeBuilding.h"
#include <vector>
#include "CoreMinimal.h"
using namespace std;

int Maze::getMazeX() {
    return mazex;
}
int Maze::getMazeY() {
    return mazey;
}

void Maze::PrintMaze(){
    for (int i=0;i<mazex;i++)
	{
        FString mazeRow = "";
		for (int j=0;j<mazey;j++)
		{
		    mazeRow+=maze[i][j];
	    }
        UE_LOG(LogTemp, Warning, TEXT("%s"),*mazeRow);
	}
}

FString Maze::GetMazeLine(int x){
    FString mazeRowStr = "";
    for (int j=0;j<mazey;j++)
    {        
        mazeRowStr+=maze[x][j];
        mazeRowStr+=",";
    }
    //UE_LOG(LogTemp, Warning, TEXT("%s"),*mazeRowStr);
    return mazeRowStr;
}

void Maze::SetBeginAndEnd() 
{

    // el seteo del begin y end del maze se va a hacer luego de haber llenado todo el maze
    // se ubicara al principio y al final cualquier punto descubierto y se cambiara el valor
    //int randomStartPosition = 1 + (2 * rand ()) % (mazey-1);
    //int randomEndPosition = 1 + (2 * rand ()) % (mazey-1);
    //float puntoMedio = mazey/2;   
    int randomSide = rand () % 2; // 0: izquierda, 1: derecha
    int randomStartPosition = mazey-2;    
    int randomEndPosition = 1;
    if(randomSide == 0)
    {
        randomStartPosition = 1;
        randomEndPosition = mazey-2;
    }
    // std::cout<<randomStartPosition<<" "<<randomEndPosition<<std::endl;
    // ubicamos el inicio de la partida en la penultima fila (la ultima es una barrera) y el final en la segunda fila (la primera es barrera)
    //maze[mazex-2][randomStartPosition] = 'B';
    //maze[1][randomEndPosition] = 'E';
    // NOTA: Debido a la forma de construir el nivel, se invertira el lugar del inicio y del final para que el jugador siempre tenga que ir hacia arriba.
    maze[1][randomStartPosition] = 'B';
    maze[mazex-2][randomEndPosition] = 'E';
}

void Maze::InitializeMazeBuilding() {
    // inicia rompiendo un bloque u al azar
    int RandomX = 1 + (2 * rand ()) % (mazex-1);
    int RandomY = 1 + (2 * rand ()) % (mazey-1);
    maze[RandomX][RandomY] = 'I';

    MarkAdjacentRoomsAsK(RandomX,RandomY);
}
// coloca sus bloques u adyacentes como F
void Maze::MarkAdjacentRoomsAsK(int x, int y)
{ 
    //cout<<"x: "<<x<<endl;
    //cout<<"y: "<<y<<endl;
    //cout<<"mazex: "<<mazex<<endl;
    //cout<<"mazey: "<<mazey<<endl;
    // chequeos verticales
    if(x < mazex-2 && maze[x+2][y] == 'u')
    {
        maze[x+2][y] = 'K';
    }
    if(x != 1 && maze[x-2][y] == 'u')
    {
        maze[x-2][y] = 'K';
    }

    // chequeos horizontales

    if(y < mazey-2 && maze[x][y+2] == 'u')
    {
        maze[x][y+2] = 'K';
    }
    if(y != 1 && maze[x][y-2] == 'u')
    {
        maze[x][y-2] = 'K';
    } 
}
void Maze::BuildMaze()
{
    // buscar una K aleatoria
    vector< vector< int > > coords;
    do {
        coords.clear();
        for (int i=0;i<mazex;i++)
        {
            for (int j=0;j<mazey;j++)
            {
                if(maze[i][j] == 'K'){
                    //cout<<"coord: ["<<i<<"]["<<j<<"]"<<endl;
                    vector< int > coord;
                    coord.push_back(i);
                    coord.push_back(j);
                    coords.push_back(coord);
                }
            }
        }
        if(!coords.empty()){
            //cout<<"coords: "<<coords.size()<<endl;
            int ChosenCoord = rand()%coords.size();
            vector< int > coord = coords[ChosenCoord];
            int KX = coord[0];
            int KY = coord[1];
            //cout<<"ChosenCoord: ["<<KX<<"]["<<KY<<"]"<<endl;

            // seleccionar un I adyacente aleatorio de esa K
            bool IFound = false;
            if(KX < mazex-2 && maze[KX+2][KY] == 'I'){
                IFound = true;
                maze[KX+1][KY] = ' '; // rompo el muro        
            } else if (KX != 1 && maze[KX-2][KY] == 'I'){
                IFound = true;        
                maze[KX-1][KY] = ' '; // rompo el muro
            } else if (KY < mazey-2 && maze[KX][KY+2] == 'I'){
                IFound = true;        
                maze[KX][KY+1] = ' '; // rompo el muro
            } else if (KY != 1 && maze[KX][KY-2] == 'I'){
                IFound = true;        
                maze[KX][KY-1] = ' '; // rompo el muro
            }
            if(IFound){
                maze[KX][KY] = 'I'; // marco la K como I
                MarkAdjacentRoomsAsK(KX, KY); // Marca las u adyacentes como nuevas K's
            }
        }
    } while (!coords.empty());

    // Limpio las I's
    for (int i=0;i<mazex;i++)
    {
        for (int j=0;j<mazey;j++)
        {
            if(maze[i][j] == 'I'){
                maze[i][j] = ' ';
            }
        }
    }

    // Establezco el inicio y final del laberinto
    SetBeginAndEnd();
}
