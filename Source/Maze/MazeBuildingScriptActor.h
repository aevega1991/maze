// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MazeBuilding.h"
#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "MazeBuildingScriptActor.generated.h"

/**
 * 
 */
UCLASS()
class MAZE_API AMazeBuildingScriptActor : public ALevelScriptActor
{
	GENERATED_BODY()
	public:
		//void CreateMaze();

	protected:
		UFUNCTION(BlueprintCallable, Category = LevelBlueprint)
		void CreateMaze(TArray<FString>& arr, int filas, int columnas);
		
		/*
		UFUNCTION(BlueprintCallable, Category = LevelBlueprint)
		void RebuildNavigation();
		*/
};
