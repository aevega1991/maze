// Fill out your copyright notice in the Description page of Project Settings.


#include "MazeBuildingScriptActor.h"
#include "CoreMinimal.h"
#include "NavigationSystem.h"

void AMazeBuildingScriptActor::CreateMaze(TArray<FString>& arr, int filas = 6, int columnas = 6)
{
    Maze nuevoMaze(filas,columnas);
    nuevoMaze.InitializeMazeBuilding();
    nuevoMaze.BuildMaze();
    //nuevoMaze.PrintMaze();
    int mazex = nuevoMaze.getMazeX();
    for (int i=0;i<mazex;i++){
        FString Line = nuevoMaze.GetMazeLine(i);
        //UE_LOG(LogTemp, Warning, TEXT("%s"),*Line);
        arr.Add(Line);
    }   
    
    UE_LOG(LogTemp, Warning, TEXT("Se genero el maze mediante codigo correctamente"));
}

/*
void AMazeBuildingScriptActor::RebuildNavigation()
{
    UNavigationSystemV1* navigation_system = UNavigationSystemV1::GetCurrent(GetWorld());
 
    if (navigation_system)
        navigation_system->Build();
}
*/