#pragma once
#include <assert.h>
#include <ctime>

class Maze {
  private:
    // Private attribute
    int filas;
    int columnas;
    char **maze;
    int mazex;
  	int mazey;

  public:
    // Constructor
    Maze(int filas, int columnas){
        srand((unsigned int)time(NULL));
        mazex =  (filas*2)+1; 
        mazey = (columnas*2)+1;

        // Asigna los espacios de memoria y crea dinamicamente el
        // array del laberinto
        char** array = new char*[mazex];  // esto asignara 'mazex' veces 'char *'s
        for (int i = 0; i < mazex; i++) {
            array[i] = new char[mazey];  // esto asignara 'mazey' veces 'char's
        }
        maze = array; // seteo el valor de mi maze privado        

        // Pinta el laberinto y lo inicializa
        
        for (int i=0;i<mazex;i++)
        {
            for (int j=0;j<mazey;j++)
            {
                if(i%2==0 || j%2==0)
                {
                    maze[i][j] = '*';              	
                }
                else
                {
                    maze[i][j] = 'u';
                }                
            }
        }
    }
    int getMazeX();
    int getMazeY();

    // Varios    
    void PrintMaze();
    void SetBeginAndEnd();
    void InitializeMazeBuilding();
    void MarkAdjacentRoomsAsK(int x, int y);
    void BuildMaze();
    FString GetMazeLine(int x);
    
};