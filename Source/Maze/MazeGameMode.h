// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MazeGameMode.generated.h"

UCLASS(minimalapi)
class AMazeGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMazeGameMode();

	//UPROPERTY(EditDefaultsOnly, Category = "Spawning")
	

protected:
	UFUNCTION()
	void SpawnObject();
	// Esta implementacion se hace en el game mode ArcadeMazeGameMode en blueprint
};



